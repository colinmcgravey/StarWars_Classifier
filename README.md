### Star Wars Machine Learning Model ###

This repo contains the files used to prep data for and build 
a neural network that developed the ability to distinguish the 
difference between images of Anakin Skywalker, Darth Vader, and 
Chewbacca. 

The model architecture can be viewed in the helpers.py file, in the
<get_model()> function. The network is a Convolutional Network, and
makes use of multiple different layer types, including MaxPooling 
and Dropout to achieve an accuracy score of 99.6% after 15 epochs. 

The data for this project was scraped from Google Images using the 
Webscraper also featured in my repository titled WebScraper. The images
were then augmented using an ImageDataGenerator from Tensorflow's data
preprocessing module. 

The file test.py contains the script necessary to test the model,
simply load in the model using the <load_model()> command from 
keras, and use the images given in the test folder. Additional 
images can be added for additional testing. The file main.py can 
be used to retrain the model, or to gain insight into how the data
was prepared to be fed into the model. 

May the Force Be With You

Code written by Colin McGravey 
